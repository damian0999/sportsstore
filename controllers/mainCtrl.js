angular.module('sportsStore')
.constant('dataUrl', 'http://localhost:5500/products')
.constant('orderUrl', 'http://localhost:5500/orders')
.controller('mainCtrl', function ($scope, $http, $location, dataUrl, orderUrl, cart){

  $scope.data = {};

  $http.get(dataUrl)
    .success(function (data){
        $scope.data.products = data;
    })
    .error(function (error){
        $scope.data.error = error;
    });

    $scope.sendOrder = function (shippingDetails) {
      var order = angular.copy(shippingDetails);
      order.products = cart.getProducts();
      $http.post(orderUrl, order)
      .success(function (data) {
        $scope.data.orderId = data.id;
        $scope.data.orderName = data.name;
        $scope.data.orderStreet = data.street;
        $scope.data.orderCity = data.city;
        $scope.data.orderState = data.state;
        $scope.data.orderZip = data.zip;
        $scope.data.orderCountry = data.country;
        cart.getProducts().length = 0;
      })
      .error(function (error) {
        $scope.data.orderError = error;
      }).finally(function () {
        $location.path("/complete");
      });
    }
});