angular.module('sportsStoreAdmin')
.constant('regisAdmin', 'http://localhost:5500/users')
.controller('registerAdminCtrl', function ($scope, $location, $http, regisAdmin ){
  $scope.registerAdmin = function(user, pass){
    $http.post(regisAdmin, {
      username: user,
      password: pass
    }).success(function (data){
      $location.path('/completeRegister')
    }).error(function (error){
      $scope.registrationError = error;
    });
  }
});